/This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function sliderXlistener(){
    //get the slider from my HTML
    
    let sliderX=document.querySelector("#rangeX");
    // get the left circle from my HTML
    // IN CS means assignment.S0 we assignment things on the right to the things on the left
    let circle=document.querySelector("#circle");

    //setAttribute means change attribute a new value
    //attribute is cx
    //value here is sliderX.value
    circle.setAttribute("cx",sliderX.value);
    circle.setAttribute("r",sliderX.value);
    console.log(circle);
}

function hello(){
    console.log("hello");
}
